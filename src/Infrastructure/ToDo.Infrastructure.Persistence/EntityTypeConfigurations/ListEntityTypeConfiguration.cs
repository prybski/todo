﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ToDo.Core.Domain.Entities;

namespace ToDo.Infrastructure.Persistence.EntityTypeConfigurations
{
    internal class ListEntityTypeConfiguration : IEntityTypeConfiguration<List>
    {
        public void Configure(EntityTypeBuilder<List> builder)
        {
            builder.Property(e => e.Name).HasMaxLength(100);
        }
    }
}