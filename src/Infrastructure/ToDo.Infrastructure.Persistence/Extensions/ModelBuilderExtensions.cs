﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.EntityFrameworkCore;

namespace ToDo.Infrastructure.Persistence.Extensions
{
    internal static class ModelBuilderExtensions
    {
        private static readonly MethodInfo ApplyQueryFilterInternalMethod = typeof(ModelBuilderExtensions)
            .GetMethods(BindingFlags.NonPublic | BindingFlags.Static)
            .Single(t => t.IsGenericMethod && t.Name == nameof(ApplyQueryFilterInternal));

        public static void ApplyUserIdConfigurationForDerivedEntities<TEntityInterface>(this ModelBuilder builder)
        {
            foreach (var entityType in builder.Model.GetEntityTypes().Where(t => t.BaseType == null)
                .Select(t => t.ClrType).Where(t => typeof(TEntityInterface).IsAssignableFrom(t)))
            {
                var entityTypeBuilder = builder.Entity(entityType);

                entityTypeBuilder.HasIndex("UserId");
            }
        }

        public static void ApplyQueryFilterForDerivedEntities<TEntityInterface>(this ModelBuilder builder,
            Expression<Func<TEntityInterface, bool>> filterExpression)
        {
            foreach (var entityType in builder.Model.GetEntityTypes().Where(t => t.BaseType == null)
                .Select(t => t.ClrType).Where(t => typeof(TEntityInterface).IsAssignableFrom(t)))
            {
                builder.ApplyQueryFilter(entityType, filterExpression);
            }
        }

        private static void ApplyQueryFilterInternal<TEntityInterface, TEntity>(this ModelBuilder builder,
            Expression<Func<TEntityInterface, bool>> filterExpression) where TEntityInterface : class
            where TEntity : class, TEntityInterface
        {
            var convertedExpression = filterExpression.Convert<TEntityInterface, TEntity>();

            builder.Entity<TEntity>().HasQueryFilter(convertedExpression);
        }

        private static void ApplyQueryFilter<TEntityInterface>(this ModelBuilder builder, Type entityType,
            Expression<Func<TEntityInterface, bool>> filterExpression)
        {
            ApplyQueryFilterInternalMethod.MakeGenericMethod(typeof(TEntityInterface), entityType)
                .Invoke(null, new object[] {builder, filterExpression});
        }
    }
}