﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ToDo.Core.Application;
using ToDo.Core.Application.Contexts.Interfaces;
using ToDo.Infrastructure.Persistence.Contexts;

namespace ToDo.Infrastructure.Persistence.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddPersistenceInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ToDoDbContext>(options =>
                options.UseNpgsql(configuration.GetConnectionString(Constants.ConnectionStrings.PostgreSql),
                    builder => builder.MigrationsHistoryTable(HistoryRepository.DefaultTableName, "migrations")));

            services.AddScoped<IToDoDbContext, ToDoDbContext>();
        }
    }
}