using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ToDo.Core.Application.Services.Interfaces;
using ToDo.Core.Domain.Common.Interfaces;

namespace ToDo.Infrastructure.Persistence.Common.Contexts
{
    public class TimestampedDbContext<TDbContext> : DbContext where TDbContext : DbContext
    {
        protected string UserId => _userResolverService.GetUserId();

        private readonly IUserResolverService _userResolverService;

        public TimestampedDbContext(DbContextOptions<TDbContext> options, IUserResolverService userResolverService) :
            base(options)
        {
            _userResolverService = userResolverService;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            var entityEntries = ChangeTracker.Entries()
                .Where(ee =>
                    ee.Entity is IEntity && (ee.State == EntityState.Added || ee.State == EntityState.Modified));

            foreach (var entityEntry in entityEntries)
            {
                ((IEntity) entityEntry.Entity).ModificationDate = DateTime.UtcNow;

                if (entityEntry.State != EntityState.Added)
                {
                    continue;
                }

                ((IEntity) entityEntry.Entity).UserId = UserId;

                ((IEntity) entityEntry.Entity).CreationDate = DateTime.UtcNow;
            }

            return await base.SaveChangesAsync(cancellationToken);
        }
    }
}