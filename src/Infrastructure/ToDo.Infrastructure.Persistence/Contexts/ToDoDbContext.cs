﻿using Microsoft.EntityFrameworkCore;
using ToDo.Core.Application.Contexts.Interfaces;
using ToDo.Core.Application.Services.Interfaces;
using ToDo.Core.Domain.Common.Interfaces;
using ToDo.Core.Domain.Entities;
using ToDo.Infrastructure.Persistence.Common.Contexts;
using ToDo.Infrastructure.Persistence.EntityTypeConfigurations;
using ToDo.Infrastructure.Persistence.Extensions;

namespace ToDo.Infrastructure.Persistence.Contexts
{
    public class ToDoDbContext : TimestampedDbContext<ToDoDbContext>, IToDoDbContext
    {
        public DbSet<List> Lists { get; }
        public DbSet<Task> Tasks { get; }

        public ToDoDbContext(DbContextOptions<ToDoDbContext> options, IUserResolverService userResolverService) :
            base(options, userResolverService)
        {
            Lists = base.Set<List>();
            Tasks = base.Set<Task>();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyUserIdConfigurationForDerivedEntities<IEntity>();

            builder.ApplyQueryFilterForDerivedEntities<IEntity>(p => p.UserId == UserId);

            builder.ApplyConfiguration(new ListEntityTypeConfiguration());
            builder.ApplyConfiguration(new TaskEntityTypeConfiguration());
        }
    }
}