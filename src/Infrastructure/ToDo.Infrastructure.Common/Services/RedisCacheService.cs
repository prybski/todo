﻿using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using ToDo.Core.Application.Services.Interfaces;

namespace ToDo.Infrastructure.Common.Services
{
    public class RedisCacheService : IRedisCacheService
    {
        private readonly IDistributedCache _distributedCache;

        private readonly JsonSerializerOptions _serializerOptions;

        public RedisCacheService(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;

            _serializerOptions = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };
        }

        public async Task RemoveAsync(string key)
        {
            await _distributedCache.RemoveAsync(key);
        }

        public async Task SetAsync<TValue>(string key, TValue value)
        {
            await _distributedCache.SetStringAsync(key, JsonSerializer.Serialize(value, _serializerOptions));
        }

        public bool TryGet<TValue>(string key, out TValue? value)
        {
            var serializedValue = _distributedCache.GetString(key);

            if (string.IsNullOrWhiteSpace(serializedValue))
            {
                value = default;

                return false;
            }

            value = JsonSerializer.Deserialize<TValue>(serializedValue, _serializerOptions)!;

            return true;
        }
    }
}