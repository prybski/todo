﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using ToDo.Core.Application.Dtos.Requests;
using ToDo.Core.Application.Exceptions;
using ToDo.Core.Application.Services.Interfaces;
using ToDo.Core.Domain.Settings;

namespace ToDo.Infrastructure.Common.Services
{
    public class EmailService : IEmailService
    {
        private readonly EmailSettings _emailSettings;

        public EmailService(IOptions<EmailSettings> emailSettings)
        {
            _emailSettings = emailSettings.Value;
        }

        public async Task SendAsync(SendEmailRequest request)
        {
            try
            {
                var (to, subject, body) = request;

                var message = new MailMessage
                {
                    From = new MailAddress(_emailSettings.From),
                    Subject = subject,
                    Body = body
                };

                message.To.Add(new MailAddress(to));

                using var client = new SmtpClient
                {
                    Host = _emailSettings.Host,
                    Port = _emailSettings.Port,
                    EnableSsl = true,
                    Credentials = new NetworkCredential(_emailSettings.Username, _emailSettings.Password)
                };

                await client.SendMailAsync(message);
            }
            catch (Exception e)
            {
                throw new SendEmailException(e.Message, HttpStatusCode.BadGateway);
            }
        }
    }
}