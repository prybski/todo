﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ToDo.Core.Application;
using ToDo.Core.Application.Services.Interfaces;
using ToDo.Core.Domain.Settings;
using ToDo.Infrastructure.Common.Services;

namespace ToDo.Infrastructure.Common.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddCommonInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<EmailSettings>(configuration.GetSection(nameof(EmailSettings)));

            services.AddTransient<IEmailService, EmailService>();

            services.AddStackExchangeRedisCache(options =>
                options.Configuration = configuration.GetConnectionString(Constants.ConnectionStrings.Redis));

            services.AddScoped<IRedisCacheService, RedisCacheService>();
        }
    }
}