using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ToDo.Core.Application.Dtos.Requests;
using ToDo.Core.Application.Dtos.Responses;
using ToDo.Core.Application.Enums;
using ToDo.Core.Application.Exceptions;
using ToDo.Core.Application.Services.Interfaces;
using ToDo.Core.Application.Wrappers.Responses;
using ToDo.Core.Domain.Settings;
using ToDo.Infrastructure.Identity.Contexts;
using ToDo.Infrastructure.Identity.Entities;

using RefreshToken = ToDo.Infrastructure.Identity.Entities.UserRefreshToken;

namespace ToDo.Infrastructure.Identity.Services
{
    public class UserService : IUserService
    {
        private readonly IdentityDbContext _context;

        private readonly IEmailService _emailService;

        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;

        private readonly JwtSettings _jwtSettings;

        public UserService(IdentityDbContext context, IEmailService emailService, SignInManager<User> signInManager,
            UserManager<User> userManager,
            IOptions<JwtSettings> jwtSettings)
        {
            _context = context;

            _emailService = emailService;

            _signInManager = signInManager;
            _userManager = userManager;

            _jwtSettings = jwtSettings.Value;
        }

        public async Task<Response<AuthenticateResponse>> AuthenticateAsync(AuthenticateRequest request,
            string ipAddress)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);

            if (user == null)
            {
                throw new AuthenticateException($"No user is registered with an e-mail address ({request.Email}).",
                    HttpStatusCode.NotFound);
            }

            var result = await _signInManager.PasswordSignInAsync(user.UserName, request.Password, false, false);

            if (!result.Succeeded)
            {
                throw new AuthenticateException(
                    $"The authentication credentials were incorrect for the user with the e-mail address ({user.Email}).",
                    HttpStatusCode.BadRequest);
            }

            if (!user.EmailConfirmed)
            {
                throw new AuthenticateException(
                    $"The e-mail address ({user.Email}) has not been confirmed for the user account.",
                    HttpStatusCode.BadRequest);
            }

            AuthenticateResponse authenticateResponse;

            if (user.RefreshTokens.Any(rt => rt.IsActive))
            {
                var userRefreshToken = user.RefreshTokens.FirstOrDefault(rt => rt.IsActive);

                authenticateResponse = new AuthenticateResponse(user.Id, user.UserName, user.Email, true,
                    new JwtSecurityTokenHandler().WriteToken(await CreateSecurityToken(user)),
                    userRefreshToken?.ExpirationDate, (await _userManager.GetRolesAsync(user)).ToList())
                {
                    RefreshToken = userRefreshToken?.Token
                };
            }
            else
            {
                var createdRefreshToken = RefreshToken.Create(ipAddress);

                authenticateResponse = new AuthenticateResponse(user.Id, user.UserName, user.Email, true,
                    new JwtSecurityTokenHandler().WriteToken(await CreateSecurityToken(user)),
                    createdRefreshToken.ExpirationDate, (await _userManager.GetRolesAsync(user)).ToList())
                {
                    RefreshToken = createdRefreshToken.Token
                };

                user.RefreshTokens.Add(createdRefreshToken);

                await _context.SaveChangesAsync();
            }

            return new Response<AuthenticateResponse>(authenticateResponse,
                $@"The user named ""{user.UserName}"" has been authenticated.");
        }

        public async Task<Response<string>> ConfirmEmailAsync(ConfirmEmailRequest request)
        {
            var (userId, token) = request;

            var user = await _userManager.FindByIdAsync(userId);

            var result =
                await _userManager.ConfirmEmailAsync(user, Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(token)));

            if (!result.Succeeded)
            {
                throw new ConfirmEmailException(
                    $@"An error occurred while confirming the e-mail address of the user named ""{user.UserName}"".",
                    HttpStatusCode.BadRequest);
            }

            return new Response<string>(user.Id,
                $@"The e-mail address of the user named ""{user.UserName}"" has been confirmed.");
        }

        public async Task<Response<string>> ForgotPasswordAsync(ForgotPasswordRequest request, string resetPasswordUrl)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);

            if (user == null)
            {
                throw new ForgotPasswordException(
                    $"No user account with the given e-mail address ({request.Email}) is registered.",
                    HttpStatusCode.NotFound);
            }

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);

            resetPasswordUrl = QueryHelpers.AddQueryString(resetPasswordUrl, "token", token);

            await _emailService.SendAsync(new SendEmailRequest(request.Email, "Reset your password",
                $"If you want to reset your password, please do so by clicking on this link: {resetPasswordUrl}."));

            return new Response<string>(user.Id,
                $"A link to reset the password has been sent to the user's e-mail address ({request.Email}).");
        }

        public async Task<Response<AuthenticateResponse>> RefreshTokenAsync(string refreshToken, string ipAddress)
        {
            var user = await _context.Users.SingleOrDefaultAsync(u =>
                u.RefreshTokens.Any(rt => rt.Token == refreshToken));

            if (user == null)
            {
                throw new RefreshTokenException($"The refresh token ({refreshToken}) is not associated with any user.",
                    HttpStatusCode.NotFound);
            }

            var userRefreshToken = user.RefreshTokens.SingleOrDefault(rt => rt.Token == refreshToken);

            if (!(userRefreshToken?.IsActive).GetValueOrDefault())
            {
                throw new RefreshTokenException($"The refresh token ({refreshToken}) is not active.",
                    HttpStatusCode.BadRequest);
            }

            if ((userRefreshToken?.IsExpired).GetValueOrDefault())
            {
                throw new RefreshTokenException($"The refresh token ({refreshToken}) has expired.",
                    HttpStatusCode.BadRequest);
            }

            if (userRefreshToken != null)
            {
                userRefreshToken.RevocationIp = ipAddress;
                userRefreshToken.RevocationDate = DateTime.UtcNow;
            }

            var createdRefreshToken = RefreshToken.Create(ipAddress);

            user.RefreshTokens.Add(createdRefreshToken);

            await _context.SaveChangesAsync();

            return new Response<AuthenticateResponse>(
                new AuthenticateResponse(user.Id, user.UserName, user.Email, true,
                    new JwtSecurityTokenHandler().WriteToken(await CreateSecurityToken(user)),
                    createdRefreshToken.Token, createdRefreshToken.ExpirationDate,
                    (await _userManager.GetRolesAsync(user)).ToList()),
                $"The token for the user with the e-mail address ({user.Email}) has been refreshed.");
        }

        public async Task<Response<string>> RegisterAsync(RegisterRequest request, string confirmEmailUrl)
        {
            var (userName, email, password) = request;

            if (await _userManager.FindByNameAsync(userName) != null)
            {
                throw new RegisterException($"The username ({userName}) is already taken.",
                    HttpStatusCode.Conflict);
            }

            var user = new User
            {
                Email = email,
                UserName = userName
            };

            if (await _userManager.FindByEmailAsync(user.Email) != null)
            {
                throw new RegisterException($"The user with e-mail address ({user.Email}) is already registered.",
                    HttpStatusCode.Conflict);
            }

            var result = await _userManager.CreateAsync(user, password);

            if (!result.Succeeded)
            {
                throw new RegisterException($@"An error occurred while registering the user named ""{user.UserName}"".",
                    HttpStatusCode.BadRequest, result.Errors.Select(ie => ie.Description));
            }

            await _userManager.AddToRoleAsync(user, Roles.User.ToString());

            var token = WebEncoders.Base64UrlEncode(
                Encoding.UTF8.GetBytes(await _userManager.GenerateEmailConfirmationTokenAsync(user)));

            confirmEmailUrl = QueryHelpers.AddQueryString(confirmEmailUrl, "userId", user.Id);
            confirmEmailUrl = QueryHelpers.AddQueryString(confirmEmailUrl, "token", token);

            await _emailService.SendAsync(new SendEmailRequest(user.Email, "Confirm your registration",
                $"If you want to confirm your registration, please do so by clicking on this link: {confirmEmailUrl}."));

            return new Response<string>(user.Id,
                $@"The user has been registered with the name ""{user.UserName}"". Check your e-mail address, if you want to confirm your registration.");
        }

        public async Task<Response<string>> ResetPasswordAsync(ResetPasswordRequest request)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);

            if (user == null)
            {
                throw new ResetPasswordException(
                    $"No user account with the given e-mail address ({request.Email}) is registered.",
                    HttpStatusCode.NotFound);
            }

            var result = await _userManager.ResetPasswordAsync(user, request.Token, request.NewPassword);

            if (!result.Succeeded)
            {
                throw new ResetPasswordException(
                    $@"An error occurred while resetting the password for the user named ""{user.UserName}"".",
                    HttpStatusCode.BadRequest, result.Errors.Select(ie => ie.Description));
            }

            return new Response<string>(request.Email,
                $@"The password for the user named ""{user.UserName}"" has been reset.");
        }

        private async Task<JwtSecurityToken> CreateSecurityToken(User user)
        {
            var userClaims = await _userManager.GetClaimsAsync(user);
            var roleClaims = (await _userManager.GetRolesAsync(user)).Select(s => new Claim("roles", s));

            var ipAddress =
                (await Dns.GetHostEntryAsync(Dns.GetHostName())).AddressList
                .FirstOrDefault(ia => ia.AddressFamily == AddressFamily.InterNetwork)?.ToString() ?? string.Empty;

            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id), new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(nameof(ipAddress).ToLower(), ipAddress)
            }.Union(userClaims).Union(roleClaims);

            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.Key));

            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);

            return new JwtSecurityToken(_jwtSettings.Issuer, _jwtSettings.Audience, claims, null,
                DateTime.UtcNow.AddMinutes(_jwtSettings.Duration), signingCredentials);
        }
    }
}