using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using ToDo.Core.Application.Contexts.Seeds;
using ToDo.Core.Application.Enums;
using ToDo.Infrastructure.Identity.Entities;

namespace ToDo.Infrastructure.Identity.Contexts.Seeds
{
    public class IdentityDbContextDefaultAdministratorSeed : DbContextSeed
    {
        public override async Task ExecuteAsync()
        {
            var defaultAdministrator = new User
            {
                UserName = "admin",
                Email = "admin@admin.com",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true
            };

            var userManager = Scope?.ServiceProvider.GetRequiredService<UserManager<User>>();

            if (userManager != null)
            {
                if (!userManager.Users.All(u => u.Id != defaultAdministrator.Id))
                {
                    return;
                }

                var existingUser = await userManager.FindByEmailAsync(defaultAdministrator.Email);

                if (existingUser != null)
                {
                    return;
                }

                await userManager.CreateAsync(defaultAdministrator, "Admin123!.");

                await userManager.AddToRoleAsync(defaultAdministrator, Roles.User.ToString());
                await userManager.AddToRoleAsync(defaultAdministrator, Roles.Administrator.ToString());
            }
        }
    }
}