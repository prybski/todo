using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using ToDo.Core.Application.Contexts.Seeds;
using ToDo.Core.Application.Enums;

using Role = Microsoft.AspNetCore.Identity.IdentityRole;

namespace ToDo.Infrastructure.Identity.Contexts.Seeds
{
    public class IdentityDbContextRolesSeed : DbContextSeed
    {
        public override async Task ExecuteAsync()
        {
            var roleManager = Scope?.ServiceProvider.GetRequiredService<RoleManager<Role>>();

            if (roleManager != null)
            {
                await roleManager.CreateAsync(new Role(Roles.User.ToString()));
                await roleManager.CreateAsync(new Role(Roles.Administrator.ToString()));
            }
        }
    }
}