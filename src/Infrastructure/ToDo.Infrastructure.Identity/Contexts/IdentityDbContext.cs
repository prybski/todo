using System.Collections.Generic;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ToDo.Core.Application.Contexts.Interfaces;
using ToDo.Core.Application.Contexts.Seeds;
using ToDo.Infrastructure.Identity.Contexts.Seeds;
using ToDo.Infrastructure.Identity.Entities;
using ToDo.Infrastructure.Identity.EntityTypeConfigurations;

namespace ToDo.Infrastructure.Identity.Contexts
{
    public class IdentityDbContext : IdentityDbContext<User>, ISeedableDbContext
    {
        public ICollection<DbContextSeed> ContextSeeds { get; }

        public IdentityDbContext(DbContextOptions<IdentityDbContext> options) : base(options)
        {
            ContextSeeds = new HashSet<DbContextSeed>
            {
                new IdentityDbContextRolesSeed(), new IdentityDbContextDefaultAdministratorSeed()
            };
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.HasDefaultSchema("identity");

            builder.ApplyConfiguration(new RoleEntityTypeConfiguration());
            builder.ApplyConfiguration(new RoleClaimEntityTypeConfiguration());
            builder.ApplyConfiguration(new UserEntityTypeConfiguration());
            builder.ApplyConfiguration(new UserRoleEntityTypeConfiguration());
            builder.ApplyConfiguration(new UserClaimEntityTypeConfiguration());
            builder.ApplyConfiguration(new UserLoginEntityTypeConfiguration());
            builder.ApplyConfiguration(new UserTokenEntityTypeConfiguration());
        }
    }
}