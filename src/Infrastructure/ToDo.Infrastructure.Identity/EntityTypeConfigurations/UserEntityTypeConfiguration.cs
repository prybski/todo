using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ToDo.Infrastructure.Identity.Entities;

namespace ToDo.Infrastructure.Identity.EntityTypeConfigurations
{
    internal class UserEntityTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable(nameof(User));

            builder.HasIndex(e => e.NormalizedEmail)
                .HasDatabaseName($"IX_{nameof(User)}_{nameof(User.NormalizedEmail)}");

            builder.HasIndex(e => e.NormalizedUserName)
                .HasDatabaseName($"IX_{nameof(User)}_{nameof(User.NormalizedUserName)}").IsUnique();

            builder.OwnsMany(e => e.RefreshTokens);
        }
    }
}