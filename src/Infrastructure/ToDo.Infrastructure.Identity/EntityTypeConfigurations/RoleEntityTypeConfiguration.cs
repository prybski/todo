using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Role = Microsoft.AspNetCore.Identity.IdentityRole;

namespace ToDo.Infrastructure.Identity.EntityTypeConfigurations
{
    internal class RoleEntityTypeConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.ToTable(nameof(Role));

            builder.HasIndex(e => e.NormalizedName).HasDatabaseName($"IX_{nameof(Role)}_{nameof(Role.NormalizedName)}")
                .IsUnique();
        }
    }
}