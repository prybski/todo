using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using UserLogin = Microsoft.AspNetCore.Identity.IdentityUserLogin<string>;

namespace ToDo.Infrastructure.Identity.EntityTypeConfigurations
{
    internal class UserLoginEntityTypeConfiguration : IEntityTypeConfiguration<UserLogin>
    {
        public void Configure(EntityTypeBuilder<UserLogin> builder)
        {
            builder.ToTable(nameof(UserLogin));
        }
    }
}