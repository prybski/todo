using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using UserClaim = Microsoft.AspNetCore.Identity.IdentityUserClaim<string>;

namespace ToDo.Infrastructure.Identity.EntityTypeConfigurations
{
    internal class UserClaimEntityTypeConfiguration : IEntityTypeConfiguration<UserClaim>
    {
        public void Configure(EntityTypeBuilder<UserClaim> builder)
        {
            builder.ToTable(nameof(UserClaim));
        }
    }
}