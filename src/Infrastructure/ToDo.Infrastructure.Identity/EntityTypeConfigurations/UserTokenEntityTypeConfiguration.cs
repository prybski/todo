using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using UserToken = Microsoft.AspNetCore.Identity.IdentityUserToken<string>;

namespace ToDo.Infrastructure.Identity.EntityTypeConfigurations
{
    internal class UserTokenEntityTypeConfiguration : IEntityTypeConfiguration<UserToken>
    {
        public void Configure(EntityTypeBuilder<UserToken> builder)
        {
            builder.ToTable(nameof(UserToken));
        }
    }
}