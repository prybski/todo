using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using RoleClaim = Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>;

namespace ToDo.Infrastructure.Identity.EntityTypeConfigurations
{
    internal class RoleClaimEntityTypeConfiguration : IEntityTypeConfiguration<RoleClaim>
    {
        public void Configure(EntityTypeBuilder<RoleClaim> builder)
        {
            builder.ToTable(nameof(RoleClaim));
        }
    }
}