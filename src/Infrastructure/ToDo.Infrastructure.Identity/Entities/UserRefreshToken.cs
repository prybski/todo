using System;
using System.Security.Cryptography;

namespace ToDo.Infrastructure.Identity.Entities
{
    public class UserRefreshToken
    {
        public Guid Id { get; set; }
        public string? Token { get; private init; }
        public string? CreationIp { get; private init; }
        public string? RevocationIp { get; set; }
        public DateTime CreationDate { get; private init; }
        public DateTime ExpirationDate { get; private init; }
        public DateTime? RevocationDate { get; set; }

        public bool IsActive => RevocationDate == null && !IsExpired;
        public bool IsExpired => DateTime.UtcNow >= ExpirationDate;

        public static UserRefreshToken Create(string? ipAddress = null)
        {
            using var cryptoServiceProvider = new RNGCryptoServiceProvider();

            var randomBytes = new byte[64];

            cryptoServiceProvider.GetBytes(randomBytes);

            return new UserRefreshToken
            {
                Token = Convert.ToBase64String(randomBytes),
                CreationIp = ipAddress,
                CreationDate = DateTime.UtcNow,
                ExpirationDate = DateTime.UtcNow.AddDays(7)
            };
        }
    }
}