using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace ToDo.Infrastructure.Identity.Entities
{
    public class User : IdentityUser
    {
        public ICollection<UserRefreshToken> RefreshTokens { get; }

        public User()
        {
            RefreshTokens = new HashSet<UserRefreshToken>();
        }
    }
}