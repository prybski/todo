using System;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using ToDo.Core.Application;
using ToDo.Core.Application.Services.Interfaces;
using ToDo.Core.Domain.Settings;
using ToDo.Infrastructure.Identity.Contexts;
using ToDo.Infrastructure.Identity.Entities;
using ToDo.Infrastructure.Identity.Services;

using Role = Microsoft.AspNetCore.Identity.IdentityRole;

namespace ToDo.Infrastructure.Identity.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddIdentityInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<JwtSettings>(configuration.GetSection(nameof(JwtSettings)));

            services.AddIdentity<User, Role>().AddEntityFrameworkStores<IdentityDbContext>().AddDefaultTokenProviders();

            services.AddScoped<IUserService, UserService>();

            services.AddDbContext<IdentityDbContext>(options =>
                options.UseNpgsql(configuration.GetConnectionString(Constants.ConnectionStrings.PostgreSql),
                    builder => builder.MigrationsHistoryTable(HistoryRepository.DefaultTableName, "migrations")));

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = false;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true, ValidateIssuer = true, ValidateAudience = true,
                    ValidateLifetime = true, ClockSkew = TimeSpan.Zero,
                    ValidIssuer = configuration[$"{nameof(JwtSettings)}:{nameof(JwtSettings.Issuer)}"],
                    ValidAudience = configuration[$"{nameof(JwtSettings)}:{nameof(JwtSettings.Audience)}"],
                    IssuerSigningKey = new SymmetricSecurityKey(
                        Encoding.UTF8.GetBytes(configuration[$"{nameof(JwtSettings)}:{nameof(JwtSettings.Key)}"]))
                };
            });
        }
    }
}