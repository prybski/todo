using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ToDo.Core.Application.Extensions;
using ToDo.Infrastructure.Common.Extensions;
using ToDo.Infrastructure.Identity.Extensions;
using ToDo.Infrastructure.Persistence.Extensions;
using ToDo.Presentation.Web.Api.Extensions;

namespace ToDo.Presentation.Web.Api
{
    /// <summary>
    /// Class containing methods, which configure services and the application's request pipeline.
    /// </summary>
    public class Startup
    {
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Class constructor injecting <see cref="IConfiguration"/> interface implementation.
        /// </summary>
        /// <param name="configuration">Implementation of the <see cref="IConfiguration"/> interface.</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// Synchronous method configuring the services.
        /// </summary>
        /// <param name="services">Collection containing configured services.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplicationLayer();

            services.AddCommonInfrastructure(Configuration);

            services.AddPersistenceInfrastructure(Configuration);

            services.AddIdentityInfrastructure(Configuration);

            services.AddCors(options => options.AddDefaultPolicy(builder =>
            {
                builder.WithOrigins(Configuration["FrontendSettings:Url"]).AllowAnyHeader().AllowAnyMethod()
                    .AllowCredentials();
            }));

            services.AddHealthChecks();

            services.AddControllers();

            services.AddSwagger();

            services.AddApiVersioning(new ApiVersion(1, 0));

            services.AddHttpContextAccessor();

            services.AddServices();
        }

        /// <summary>
        /// Synchronous method configuring the application's request pipeline.
        /// </summary>
        /// <param name="app">Builder of the application's request pipeline configuration.</param>
        /// <param name="env">Web hosting environment information.</param>
        public static void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger(typeof(Startup).Assembly.GetName().Name ?? string.Empty);

            app.UseDefaultExceptionHandler();

            app.UseRouting();

            app.UseCors();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/health");

                endpoints.MapControllers();
            });
        }
    }
}