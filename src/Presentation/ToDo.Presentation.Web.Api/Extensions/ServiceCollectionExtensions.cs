using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using ToDo.Core.Application.Services.Interfaces;
using ToDo.Presentation.Web.Api.OperationFilters;
using ToDo.Presentation.Web.Api.Services;

namespace ToDo.Presentation.Web.Api.Extensions
{
    internal static class ServiceCollectionExtensions
    {
        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo {Version = "v1", Title = "ToDo.Presentation.Web.Api"});

                options.AddSecurityDefinition("Bearer",
                    new OpenApiSecurityScheme
                    {
                        Description = "JSON Web Token, which will be used to access secured endpoints.",
                        Name = "Authorization", In = ParameterLocation.Header, Scheme = "bearer",
                        Type = SecuritySchemeType.Http,
                    });

                options.OperationFilter<ApplySecurityRequirementOperationFilter>();

                var xmlFilePath = Path.Combine(AppContext.BaseDirectory,
                    $"{Assembly.GetExecutingAssembly().GetName().Name}.xml");

                options.IncludeXmlComments(xmlFilePath);
            });
        }

        public static void AddApiVersioning(this IServiceCollection services, ApiVersion defaultApiVersion)
        {
            services.AddApiVersioning(options =>
            {
                options.DefaultApiVersion = defaultApiVersion;

                options.AssumeDefaultVersionWhenUnspecified = true;

                options.ReportApiVersions = true;
            });
        }

        public static void AddServices(this IServiceCollection services)
        {
            services.AddScoped<IUserResolverService, UserResolverService>();
        }
    }
}