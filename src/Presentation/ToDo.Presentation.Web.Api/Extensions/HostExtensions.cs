﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Threading.Tasks;
using ToDo.Core.Application.Contexts.Interfaces;

namespace ToDo.Presentation.Web.Api.Extensions
{
    internal static class HostExtensions
    {
        public static IHost Migrate<TDbContext>(this IHost host) where TDbContext : DbContext
        {
            using var scope = host.Services.CreateScope();

            var provider = scope.ServiceProvider;

            var context = provider.GetRequiredService<TDbContext>();

            context.Database.Migrate();

            return host;
        }

        public static async Task<IHost> SeedAsync<TDbContext>(this IHost host)
            where TDbContext : DbContext, ISeedableDbContext
        {
            using var scope = host.Services.CreateScope();

            var provider = scope.ServiceProvider;

            var context = provider.GetRequiredService<TDbContext>();

            foreach (var contextSeed in context.ContextSeeds)
            {
                contextSeed.Scope = host.Services.CreateScope();

                await contextSeed.ExecuteAsync();
            }

            return host;
        }
    }
}