using System.Linq;
using System.Net;
using System.Text.Json;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using ToDo.Core.Application.Common.Exceptions;
using ToDo.Core.Application.Wrappers.Responses;

namespace ToDo.Presentation.Web.Api.Extensions
{
    internal static class ApplicationBuilderExtensions
    {
        public static void UseSwagger(this IApplicationBuilder app, string assemblyName)
        {
            app.UseSwagger();

            app.UseSwaggerUI(options => { options.SwaggerEndpoint("/swagger/v1/swagger.json", assemblyName); });
        }

        public static void UseDefaultExceptionHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(action => action.Run(async context =>
                    {
                        var response = context.Response;
                        response.ContentType = "application/json";

                        var exception = context.Features.Get<IExceptionHandlerPathFeature>().Error;

                        var responseWrapper = new Response<string>(exception?.Message)
                        {
                            Errors = (exception as ApiException)?.Errors ?? Enumerable.Empty<string>()
                        };

                        response.StatusCode = (int) ((exception as ApiException)?.StatusCode ??
                                                     HttpStatusCode.InternalServerError);

                        await response.WriteAsync(
                            JsonSerializer.Serialize(
                                responseWrapper,
                                new JsonSerializerOptions {PropertyNamingPolicy = JsonNamingPolicy.CamelCase}
                            )
                        );

                        await context.Response.CompleteAsync();
                    }
                )
            );
        }
    }
}