﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace ToDo.Presentation.Web.Api.Common.Controllers
{
    /// <summary>
    /// Abstract controller for API endpoints with necessary services, attributes and API versioning.
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    public abstract class ApiController : ControllerBase
    {
        /// <summary>
        /// Mediator service used for Command and Query Responsibility Segregation (CQRS) pattern.
        /// </summary>
        protected IMediator Mediator => HttpContext.RequestServices.GetRequiredService<IMediator>();
    }
}