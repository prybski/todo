﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ToDo.Core.Application.Commands.TaskCommands;
using ToDo.Core.Application.Filters;
using ToDo.Core.Application.Queries.TaskQueries;
using ToDo.Core.Domain.Entities;
using ToDo.Presentation.Web.Api.Common.Controllers;

using Task = ToDo.Core.Domain.Entities.Task;

namespace ToDo.Presentation.Web.Api.Controllers.V1
{
    /// <summary>
    /// Controller containing methods used to fetch entities of type <see cref="Task"/> and perform modifications
    /// on them.
    /// </summary>
    [ApiVersion("1.0")]
    public class TaskController : ApiController
    {
        /// <summary>
        /// Asynchronous method, which creates <see cref="Task"/> entity.
        /// </summary>
        /// <param name="command">Command containing data to create the <see cref="Task"/> entity.</param>
        /// <returns>An <see cref="ActionResult"/> object determining status of the operation.</returns>
        [HttpPost]
        public async Task<IActionResult> Create(CreateTaskCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Asynchronous method, which deletes <see cref="Task"/> entity.
        /// </summary>
        /// <param name="id">Identifier of the <see cref="Task"/> entity.</param>
        /// <returns>An <see cref="ActionResult"/> object determining status of the operation.</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteTaskCommand(id)));
        }

        /// <summary>
        /// Asynchronous method, which fetches <see cref="Task"/> entity.
        /// </summary>
        /// <param name="id">Identifier of the <see cref="Task"/> entity.</param>
        /// <returns>An <see cref="ActionResult"/> object containing fetched data and determining status of the
        /// operation.</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetTaskQuery(id)));
        }

        /// <summary>
        /// Asynchronous method, which can fetch all <see cref="Task"/> entities with pagination applied.
        /// </summary>
        /// <param name="listId">Identifier of the related <see cref="List"/> entity.</param>
        /// <param name="filter">Filter containing data about page number and page size.</param>
        /// <returns>An <see cref="ActionResult"/> object containing fetched data and determining status of the
        /// operation.</returns>
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] int? listId, [FromQuery] [Required] PaginationFilter filter)
        {
            return Ok(await Mediator.Send(new GetAllTasksPaginatedQuery(listId, filter)));
        }

        /// <summary>
        /// Asynchronous method, which updates <see cref="Task"/> entity.
        /// </summary>
        /// <param name="command">Command containing data to update the <see cref="Task"/> entity.</param>
        /// <returns>An <see cref="ActionResult"/> object determining status of the operation.</returns>
        [HttpPut]
        public async Task<IActionResult> Update(UpdateTaskCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}