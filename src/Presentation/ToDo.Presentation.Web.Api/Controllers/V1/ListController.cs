﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ToDo.Core.Application.Commands.ListCommands;
using ToDo.Core.Application.Filters;
using ToDo.Core.Application.Queries.ListQueries;
using ToDo.Core.Domain.Entities;
using ToDo.Presentation.Web.Api.Common.Controllers;

namespace ToDo.Presentation.Web.Api.Controllers.V1
{
    /// <summary>
    /// Controller containing methods used to fetch entities of type <see cref="List"/> and perform modifications on them.
    /// </summary>
    [ApiVersion("1.0")]
    public class ListController : ApiController
    {
        /// <summary>
        /// Asynchronous method, which creates <see cref="List"/> entity.
        /// </summary>
        /// <param name="command">Command containing data to create the <see cref="List"/> entity.</param>
        /// <returns>An <see cref="ActionResult"/> object determining status of the operation.</returns>
        [HttpPost]
        public async Task<IActionResult> Create(CreateListCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Asynchronous method, which deletes <see cref="List"/> entity.
        /// </summary>
        /// <param name="id">Identifier of the <see cref="List"/> entity.</param>
        /// <returns>An <see cref="ActionResult"/> object determining status of the operation.</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteListCommand(id)));
        }

        /// <summary>
        /// Asynchronous method, which fetches <see cref="List"/> entity.
        /// </summary>
        /// <param name="id">Identifier of the <see cref="List"/> entity.</param>
        /// <returns>An <see cref="ActionResult"/> object containing fetched data and determining status of the operation.</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetListQuery(id)));
        }

        /// <summary>
        /// Asynchronous method, which can fetch all <see cref="List"/> entities with pagination applied.
        /// </summary>
        /// <param name="filter">Filter containing data about page number and page size.</param>
        /// <returns>An <see cref="ActionResult"/> object containing fetched data and determining status of the operation.</returns>
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] [Required] PaginationFilter filter)
        {
            return Ok(await Mediator.Send(new GetAllListsPaginatedQuery(filter)));
        }

        /// <summary>
        /// Asynchronous method, which updates <see cref="List"/> entity.
        /// </summary>
        /// <param name="command">Command containing data to update the <see cref="List"/> entity.</param>
        /// <returns>An <see cref="ActionResult"/> object determining status of the operation.</returns>
        [HttpPut]
        public async Task<IActionResult> Update(UpdateListCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}