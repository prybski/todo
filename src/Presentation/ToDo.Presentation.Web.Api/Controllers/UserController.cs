using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ToDo.Core.Application.Dtos.Requests;
using ToDo.Core.Application.Services.Interfaces;

namespace ToDo.Presentation.Web.Api.Controllers
{
    /// <summary>
    /// Controller containing methods used to perform operations related to ASP.NET Core Identity membership system.
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        private readonly IUserService _userService;

        /// <summary>
        /// Controller constructor injecting the necessary interfaces implementations.
        /// </summary>
        /// <param name="configuration">Set of key/value application configuration properties.</param>
        /// <param name="userService">Implementation of the <see cref="IUserService"/> interface.</param>
        public UserController(IConfiguration configuration, IUserService userService)
        {
            _configuration = configuration;

            _userService = userService;
        }

        /// <summary>
        /// Asynchronous method, which authenticates the user based on the provided credentials.
        /// </summary>
        /// <param name="request">Request containing credentials to authenticate the user.</param>
        /// <returns>An <see cref="ActionResult"/> object determining status of the operation.</returns>
        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<IActionResult> Authenticate([FromQuery] AuthenticateRequest request)
        {
            var response = await _userService.AuthenticateAsync(request,
                (Request.Headers.ContainsKey("X-Forwarded-For")
                    ? Request.Headers["X-Forwarded-For"].ToString()
                    : HttpContext.Connection.RemoteIpAddress?.MapToIPv4().ToString()) ?? string.Empty);

            Response.Cookies.Append("_refreshToken", response.Data?.RefreshToken ?? string.Empty,
                new CookieOptions {HttpOnly = true, Expires = response.Data?.RefreshTokenExpirationDate});

            return Ok(response);
        }

        /// <summary>
        /// Asynchronous method, which confirms e-mail address of the user.
        /// </summary>
        /// <param name="request">Request containing data to confirm user's e-mail address.</param>
        /// <returns>An <see cref="ActionResult"/> object determining status of the operation.</returns>
        [AllowAnonymous]
        [HttpPut("[action]")]
        public async Task<IActionResult> ConfirmEmail(ConfirmEmailRequest request)
        {
            return Ok(await _userService.ConfirmEmailAsync(request));
        }

        /// <summary>
        /// Asynchronous method, which generates and sends password reset token to the registered user's e-mail address.
        /// </summary>
        /// <param name="request">Request containing data to generate and send password reset token to the registered
        /// user's e-mail address.</param>
        /// <returns>An <see cref="ActionResult"/> object determining status of the operation.</returns>
        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<IActionResult> ForgotPassword([FromQuery] ForgotPasswordRequest request)
        {
            return Ok(await _userService.ForgotPasswordAsync(request,
                $"{_configuration["FrontendSettings:Url"]}/reset-password"));
        }

        /// <summary>
        /// Asynchronous method, which refreshes the token and authenticates the user.
        /// </summary>
        /// <returns>An <see cref="ActionResult"/> object determining status of the operation.</returns>
        [AllowAnonymous]
        [HttpPost("[action]")]
        public async Task<IActionResult> RefreshToken()
        {
            var refreshToken = Request.Cookies["_refreshToken"];

            var response = await _userService.RefreshTokenAsync(refreshToken ?? string.Empty,
                (Request.Headers.ContainsKey("X-Forwarded-For")
                    ? Request.Headers["X-Forwarded-For"].ToString()
                    : HttpContext.Connection.RemoteIpAddress?.MapToIPv4().ToString()) ?? string.Empty);

            if (!string.IsNullOrWhiteSpace(response.Data?.RefreshToken))
            {
                Response.Cookies.Append("_refreshToken", response.Data.RefreshToken,
                    new CookieOptions {HttpOnly = true, Expires = response.Data.RefreshTokenExpirationDate});
            }

            return Ok(response);
        }

        /// <summary>
        /// Asynchronous method, which registers the user.
        /// </summary>
        /// <param name="request">Request containing data to register the user.</param>
        /// <returns>An <see cref="ActionResult"/> object determining status of the operation.</returns>
        [AllowAnonymous]
        [HttpPost("[action]")]
        public async Task<IActionResult> Register(RegisterRequest request)
        {
            return StatusCode(StatusCodes.Status201Created,
                await _userService.RegisterAsync(request, $"{_configuration["FrontendSettings:Url"]}/confirm-email"));
        }

        /// <summary>
        /// Asynchronous method, which resets a user's password.
        /// </summary>
        /// <param name="request">Request containing data to reset a user's password.</param>
        /// <returns>An <see cref="ActionResult"/> object determining status of the operation.</returns>
        [AllowAnonymous]
        [HttpPut("[action]")]
        public async Task<IActionResult> ResetPassword(ResetPasswordRequest request)
        {
            return Ok(await _userService.ResetPasswordAsync(request));
        }
    }
}