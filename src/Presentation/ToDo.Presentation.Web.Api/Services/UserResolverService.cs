﻿using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using ToDo.Core.Application.Services.Interfaces;
using ToDo.Infrastructure.Identity.Entities;

namespace ToDo.Presentation.Web.Api.Services
{
    /// <summary>
    /// Service containing implementation of <see cref="IUserResolverService"/> interface method used to retrieve the
    /// <see cref="User"/> identifier.
    /// </summary>
    public class UserResolverService : IUserResolverService
    {
        private readonly IHttpContextAccessor _contextAccessor;

        /// <summary>
        /// Service constructor injecting the necessary interfaces implementations.
        /// </summary>
        /// <param name="contextAccessor">Implementation of the <see cref="IHttpContextAccessor"/> interface providing
        /// access to the current HttpContext.</param>
        public UserResolverService(IHttpContextAccessor contextAccessor)
        {
            _contextAccessor = contextAccessor;
        }

        /// <summary>
        /// Synchronous method, which gets the identifier of the authenticated <see cref="User"/>.
        /// </summary>
        /// <returns>An identifier of the authenticated <see cref="User"/>.</returns>
        public string GetUserId()
        {
            return _contextAccessor.HttpContext?.User.FindFirstValue(ClaimTypes.NameIdentifier) ??
                   Guid.Empty.ToString();
        }
    }
}