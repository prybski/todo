using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace ToDo.Presentation.Web.Api.OperationFilters
{
    internal class ApplySecurityRequirementOperationFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext filterContext)
        {
            var declaringTypeAuthorizeAttributes = filterContext.MethodInfo.DeclaringType?.GetCustomAttributes(true)
                .OfType<AuthorizeAttribute>();

            var methodAllowAnonymousAttributes =
                filterContext.MethodInfo.GetCustomAttributes(true).OfType<AllowAnonymousAttribute>();

            if ((declaringTypeAuthorizeAttributes?.Any()).GetValueOrDefault() && !methodAllowAnonymousAttributes.Any())
            {
                operation.Security = new List<OpenApiSecurityRequirement>
                {
                    new()
                    {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Id = "Bearer",
                                    Type = ReferenceType.SecurityScheme
                                },
                            },
                            new List<string>()
                        }
                    }
                };
            }
        }
    }
}