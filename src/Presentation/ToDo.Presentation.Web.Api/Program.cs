using System;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Sinks.Elasticsearch;
using ToDo.Infrastructure.Identity.Contexts;
using ToDo.Infrastructure.Persistence.Contexts;
using ToDo.Presentation.Web.Api.Extensions;

namespace ToDo.Presentation.Web.Api
{
    /// <summary>
    /// Class containing methods required to launch the host.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Asynchronous static method, which launches the host.
        /// </summary>
        /// <param name="args">Additional arguments, that can be passed in during launching of the host.</param>
        /// <returns>A <see cref="Task"/> representing the launch operation.</returns>
        public static async Task Main(string[] args)
        {
            ConfigureSerilog();

            try
            {
                var host = CreateHostBuilder(args).Build().Migrate<IdentityDbContext>().Migrate<ToDoDbContext>();

                host = await host.SeedAsync<IdentityDbContext>();

                await host.RunAsync();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, ex.Message);
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureWebHostDefaults(builder => { builder.UseStartup<Startup>(); });
        }

        private static void ConfigureSerilog()
        {
            var configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json", false, true)
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", true,
                    true).AddEnvironmentVariables().Build();

            Log.Logger = new LoggerConfiguration().WriteTo
                .Elasticsearch(new ElasticsearchSinkOptions(new Uri(configuration["ElasticsearchSettings:Url"]))
                {
                    AutoRegisterTemplate = true,
                    IndexFormat =
                        $"{Assembly.GetExecutingAssembly().GetName().Name?.ToLower()}-{DateTime.UtcNow:yyyy-MM}"
                }).Enrich.WithProperty("EnvironmentName", Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"))
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }
    }
}