﻿using System;

namespace ToDo.Core.Domain.Common.Interfaces
{
    public interface IEntity
    {
        int Id { get; set; }
        string UserId { get; set; }
        DateTime CreationDate { get; set; }
        DateTime ModificationDate { get; set; }
    }
}