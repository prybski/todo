namespace ToDo.Core.Domain.Settings
{
    public record JwtSettings(string Key, string Issuer, string Audience, double Duration)
    {
        public JwtSettings() : this(string.Empty, string.Empty, string.Empty, default)
        {
        }
    }
}