﻿namespace ToDo.Core.Domain.Settings
{
    public record EmailSettings(string From, string Host, int Port, string Username, string Password)
    {
        public EmailSettings() : this(string.Empty, string.Empty, default, string.Empty, string.Empty)
        {
        }
    }
}