﻿using System;
using ToDo.Core.Domain.Common.Interfaces;
using ToDo.Core.Domain.Enums;

namespace ToDo.Core.Domain.Entities
{
    public sealed class Task : IEntity
    {
        public int Id { get; set; }
        public string UserId { get; set; } = null!;
        public int ListId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public TaskType? Type { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ModificationDate { get; set; }

        public List List { get; set; } = null!;

        public Task(string title, string description, TaskType? type)
        {
            Title = title;
            Description = description;
            Type = type;
        }
    }
}