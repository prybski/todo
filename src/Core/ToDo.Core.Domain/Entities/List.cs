﻿using System;
using System.Collections.Generic;
using ToDo.Core.Domain.Common.Interfaces;

namespace ToDo.Core.Domain.Entities
{
    public sealed class List : IEntity
    {
        public int Id { get; set; }
        public string UserId { get; set; } = null!;
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ModificationDate { get; set; }

        public ICollection<Task> Tasks { get; }

        public List(string name)
        {
            Name = name;

            Tasks = new HashSet<Task>();
        }
    }
}