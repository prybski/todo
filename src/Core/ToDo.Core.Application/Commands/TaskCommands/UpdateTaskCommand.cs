﻿using System.ComponentModel.DataAnnotations;
using MediatR;
using ToDo.Core.Domain.Enums;

namespace ToDo.Core.Application.Commands.TaskCommands
{
    public record UpdateTaskCommand
        (int Id, [Required] string Title, [Required] string Description, TaskType? Type) : IRequest<int>;
}