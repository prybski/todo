﻿using MediatR;

namespace ToDo.Core.Application.Commands.TaskCommands
{
    public record DeleteTaskCommand(int Id) : IRequest<int>;
}