﻿using System.ComponentModel.DataAnnotations;
using MediatR;
using ToDo.Core.Domain.Enums;

namespace ToDo.Core.Application.Commands.TaskCommands
{
    public record CreateTaskCommand
        (int ListId, [Required] string Title, [Required] string Description, TaskType? Type) : IRequest<int>;
}