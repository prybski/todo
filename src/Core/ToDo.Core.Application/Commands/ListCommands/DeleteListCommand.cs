﻿using MediatR;

namespace ToDo.Core.Application.Commands.ListCommands
{
    public record DeleteListCommand(int Id) : IRequest<int>;
}