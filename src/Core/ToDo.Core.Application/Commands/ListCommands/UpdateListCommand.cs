﻿using System.ComponentModel.DataAnnotations;
using MediatR;

namespace ToDo.Core.Application.Commands.ListCommands
{
    public record UpdateListCommand(int Id, [Required] string Name) : IRequest<int>;
}