﻿using System.ComponentModel.DataAnnotations;
using MediatR;

namespace ToDo.Core.Application.Commands.ListCommands
{
    public record CreateListCommand([Required] string Name) : IRequest<int>;
}