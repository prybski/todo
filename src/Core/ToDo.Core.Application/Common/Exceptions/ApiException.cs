using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace ToDo.Core.Application.Common.Exceptions
{
    public class ApiException : Exception
    {
        public HttpStatusCode StatusCode { get; }

        public IEnumerable<string> Errors { get; }

        protected ApiException(string message, HttpStatusCode statusCode) : base(message)
        {
            StatusCode = statusCode;

            Errors = Enumerable.Empty<string>();
        }

        protected ApiException(string message, HttpStatusCode statusCode, IEnumerable<string> errors) : this(message,
            statusCode)
        {
            Errors = errors;
        }
    }
}