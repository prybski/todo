namespace ToDo.Core.Application.Enums
{
    public enum Roles
    {
        Administrator,
        User
    }
}