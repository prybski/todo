﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ToDo.Core.Application.Commands.TaskCommands;
using ToDo.Core.Application.Contexts.Interfaces;

using Task = ToDo.Core.Domain.Entities.Task;

namespace ToDo.Core.Application.CommandHandlers.TaskCommandHandlers
{
    public class CreateTaskCommandHandler : IRequestHandler<CreateTaskCommand, int>
    {
        private readonly IToDoDbContext _context;

        public CreateTaskCommandHandler(IToDoDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateTaskCommand command, CancellationToken cancellationToken)
        {
            var list = await _context.Lists.Include(l => l.Tasks)
                .FirstOrDefaultAsync(l => l.Id == command.ListId, cancellationToken: cancellationToken);

            if (list == null)
            {
                return default;
            }

            var task = new Task(command.Title, command.Description, command.Type);

            list.Tasks.Add(task);

            await _context.SaveChangesAsync(cancellationToken);

            return task.Id;
        }
    }
}