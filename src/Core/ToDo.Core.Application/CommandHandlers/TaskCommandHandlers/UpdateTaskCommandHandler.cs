﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ToDo.Core.Application.Commands.TaskCommands;
using ToDo.Core.Application.Contexts.Interfaces;
using ToDo.Core.Application.Services.Interfaces;

using Task = ToDo.Core.Domain.Entities.Task;

namespace ToDo.Core.Application.CommandHandlers.TaskCommandHandlers
{
    public class UpdateTaskCommandHandler : IRequestHandler<UpdateTaskCommand, int>
    {
        private readonly IToDoDbContext _context;

        private readonly IRedisCacheService _cacheService;

        public UpdateTaskCommandHandler(IToDoDbContext context, IRedisCacheService cacheService)
        {
            _context = context;

            _cacheService = cacheService;
        }

        public async Task<int> Handle(UpdateTaskCommand command, CancellationToken cancellationToken)
        {
            var task = await _context.Tasks.FirstOrDefaultAsync(t => t.Id == command.Id, cancellationToken);

            if (task == null)
            {
                return default;
            }

            task.Title = command.Title;
            task.Description = command.Description;
            task.Type = command.Type;

            await _context.SaveChangesAsync(cancellationToken);

            if (_cacheService.TryGet(Constants.CacheKeys.CachedTask, out Task? cachedTask) &&
                cachedTask?.Id == command.Id)
            {
                await _cacheService.SetAsync(Constants.CacheKeys.CachedTask, task);
            }

            return task.Id;
        }
    }
}