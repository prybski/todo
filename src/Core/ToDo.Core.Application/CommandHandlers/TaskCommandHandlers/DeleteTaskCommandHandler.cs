﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ToDo.Core.Application.Commands.TaskCommands;
using ToDo.Core.Application.Contexts.Interfaces;

namespace ToDo.Core.Application.CommandHandlers.TaskCommandHandlers
{
    public class DeleteTaskCommandHandler : IRequestHandler<DeleteTaskCommand, int>
    {
        private readonly IToDoDbContext _context;

        public DeleteTaskCommandHandler(IToDoDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(DeleteTaskCommand command, CancellationToken cancellationToken)
        {
            var task = await _context.Tasks.FirstOrDefaultAsync(t => t.Id == command.Id, cancellationToken);

            if (task == null)
            {
                return default;
            }

            _context.Tasks.Remove(task);

            await _context.SaveChangesAsync(cancellationToken);

            return task.Id;
        }
    }
}