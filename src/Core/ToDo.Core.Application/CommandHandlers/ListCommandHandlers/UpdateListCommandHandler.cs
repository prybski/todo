﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ToDo.Core.Application.Commands.ListCommands;
using ToDo.Core.Application.Contexts.Interfaces;
using ToDo.Core.Application.Services.Interfaces;
using ToDo.Core.Domain.Entities;

namespace ToDo.Core.Application.CommandHandlers.ListCommandHandlers
{
    public class UpdateListCommandHandler : IRequestHandler<UpdateListCommand, int>
    {
        private readonly IToDoDbContext _context;

        private readonly IRedisCacheService _cacheService;

        public UpdateListCommandHandler(IToDoDbContext context, IRedisCacheService cacheService)
        {
            _context = context;

            _cacheService = cacheService;
        }

        public async Task<int> Handle(UpdateListCommand command, CancellationToken cancellationToken)
        {
            var (id, name) = command;

            var list = await _context.Lists.FirstOrDefaultAsync(l => l.Id == id, cancellationToken);

            if (list == null)
            {
                return default;
            }

            list.Name = name;

            await _context.SaveChangesAsync(cancellationToken);

            if (_cacheService.TryGet(Constants.CacheKeys.CachedList, out List? cachedList) &&
                cachedList?.Id == command.Id)
            {
                await _cacheService.SetAsync(Constants.CacheKeys.CachedList, list);
            }

            return list.Id;
        }
    }
}