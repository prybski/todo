﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ToDo.Core.Application.Commands.ListCommands;
using ToDo.Core.Application.Contexts.Interfaces;
using ToDo.Core.Domain.Entities;

namespace ToDo.Core.Application.CommandHandlers.ListCommandHandlers
{
    public class CreateListCommandHandler : IRequestHandler<CreateListCommand, int>
    {
        private readonly IToDoDbContext _context;

        public CreateListCommandHandler(IToDoDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateListCommand command, CancellationToken cancellationToken)
        {
            var list = new List(command.Name);

            await _context.Lists.AddAsync(list, cancellationToken);

            await _context.SaveChangesAsync(cancellationToken);

            return list.Id;
        }
    }
}