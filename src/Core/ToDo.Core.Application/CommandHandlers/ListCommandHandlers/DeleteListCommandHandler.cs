﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ToDo.Core.Application.Commands.ListCommands;
using ToDo.Core.Application.Contexts.Interfaces;

namespace ToDo.Core.Application.CommandHandlers.ListCommandHandlers
{
    public class DeleteListCommandHandler : IRequestHandler<DeleteListCommand, int>
    {
        private readonly IToDoDbContext _context;

        public DeleteListCommandHandler(IToDoDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(DeleteListCommand command, CancellationToken cancellationToken)
        {
            var list = await _context.Lists.FirstOrDefaultAsync(l => l.Id == command.Id, cancellationToken);

            if (list == null)
            {
                return default;
            }

            _context.Lists.Remove(list);

            await _context.SaveChangesAsync(cancellationToken);

            return list.Id;
        }
    }
}