﻿using MediatR;
using ToDo.Core.Application.Dtos.Responses;
using ToDo.Core.Application.Wrappers.Responses;

namespace ToDo.Core.Application.Queries.ListQueries
{
    public record GetListQuery(int Id) : IRequest<Response<GetListResponse>>;
}