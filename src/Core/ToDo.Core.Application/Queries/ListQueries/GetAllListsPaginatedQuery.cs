﻿using MediatR;
using System.Collections.Generic;
using ToDo.Core.Application.Dtos.Responses;
using ToDo.Core.Application.Filters;
using ToDo.Core.Application.Wrappers.Responses;

namespace ToDo.Core.Application.Queries.ListQueries
{
    public record GetAllListsPaginatedQuery
        (PaginationFilter Filter) : IRequest<PaginatedResponse<IEnumerable<GetListResponse>>>;
}