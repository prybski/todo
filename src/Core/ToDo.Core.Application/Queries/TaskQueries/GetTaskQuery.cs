﻿using MediatR;
using ToDo.Core.Application.Dtos.Responses;
using ToDo.Core.Application.Wrappers.Responses;

namespace ToDo.Core.Application.Queries.TaskQueries
{
    public record GetTaskQuery(int Id) : IRequest<Response<GetTaskResponse>>;
}