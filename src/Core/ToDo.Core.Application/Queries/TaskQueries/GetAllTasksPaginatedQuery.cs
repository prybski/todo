﻿using System.Collections.Generic;
using MediatR;
using ToDo.Core.Application.Dtos.Responses;
using ToDo.Core.Application.Filters;
using ToDo.Core.Application.Wrappers.Responses;

namespace ToDo.Core.Application.Queries.TaskQueries
{
    public record GetAllTasksPaginatedQuery
        (int? ListId, PaginationFilter Filter) : IRequest<PaginatedResponse<IEnumerable<GetTaskResponse>>>;
}