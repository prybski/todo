namespace ToDo.Core.Application.Filters
{
    public record PaginationFilter(int PageNumber = 1, int PageSize = 10);
}