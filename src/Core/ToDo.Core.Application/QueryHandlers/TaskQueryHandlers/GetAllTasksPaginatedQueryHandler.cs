﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ToDo.Core.Application.Contexts.Interfaces;
using ToDo.Core.Application.Dtos.Responses;
using ToDo.Core.Application.Extensions;
using ToDo.Core.Application.Queries.TaskQueries;
using ToDo.Core.Application.Wrappers.Responses;

namespace ToDo.Core.Application.QueryHandlers.TaskQueryHandlers
{
    public class GetAllTasksPaginatedQueryHandler : IRequestHandler<GetAllTasksPaginatedQuery,
        PaginatedResponse<IEnumerable<GetTaskResponse>>>
    {
        private readonly IToDoDbContext _context;

        public GetAllTasksPaginatedQueryHandler(IToDoDbContext context)
        {
            _context = context;
        }

        public async Task<PaginatedResponse<IEnumerable<GetTaskResponse>>> Handle(GetAllTasksPaginatedQuery query,
            CancellationToken cancellationToken)
        {
            var (listId, paginationFilter) = query;

            var tasks = _context.Tasks.OrderBy(t => t.Id);

            if (!listId.HasValue)
            {
                return await tasks.Select(t => GetTaskResponse.From(t))
                    .AsPaginatedResponseAsync(paginationFilter);
            }

            return await tasks.Where(t => t.ListId == listId).Select(t => GetTaskResponse.From(t))
                .AsPaginatedResponseAsync(paginationFilter);
        }
    }
}