﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ToDo.Core.Application.Contexts.Interfaces;
using ToDo.Core.Application.Dtos.Responses;
using ToDo.Core.Application.Queries.TaskQueries;
using ToDo.Core.Application.Services.Interfaces;
using ToDo.Core.Application.Wrappers.Responses;

using Task = ToDo.Core.Domain.Entities.Task;

namespace ToDo.Core.Application.QueryHandlers.TaskQueryHandlers
{
    public class GetTaskQueryHandler : IRequestHandler<GetTaskQuery, Response<GetTaskResponse>>
    {
        private readonly IToDoDbContext _context;

        private readonly IRedisCacheService _cacheService;

        public GetTaskQueryHandler(IToDoDbContext context, IRedisCacheService cacheService)
        {
            _context = context;

            _cacheService = cacheService;
        }

        public async Task<Response<GetTaskResponse>> Handle(GetTaskQuery query, CancellationToken cancellationToken)
        {
            if (_cacheService.TryGet(Constants.CacheKeys.CachedTask, out Task? cachedTask) &&
                cachedTask?.Id == query.Id)
            {
                return new Response<GetTaskResponse>(GetTaskResponse.From(cachedTask));
            }

            var task = await _context.Tasks.FirstOrDefaultAsync(t => t.Id == query.Id, cancellationToken);

            await _cacheService.SetAsync(Constants.CacheKeys.CachedTask, task);

            return new Response<GetTaskResponse>(GetTaskResponse.From(task));
        }
    }
}