﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ToDo.Core.Application.Contexts.Interfaces;
using ToDo.Core.Application.Dtos.Responses;
using ToDo.Core.Application.Queries.ListQueries;
using ToDo.Core.Application.Services.Interfaces;
using ToDo.Core.Application.Wrappers.Responses;
using ToDo.Core.Domain.Entities;

namespace ToDo.Core.Application.QueryHandlers.ListQueryHandlers
{
    public class GetListQueryHandler : IRequestHandler<GetListQuery, Response<GetListResponse>>
    {
        private readonly IToDoDbContext _context;

        private readonly IRedisCacheService _cacheService;

        public GetListQueryHandler(IToDoDbContext context, IRedisCacheService cacheService)
        {
            _context = context;

            _cacheService = cacheService;
        }

        public async Task<Response<GetListResponse>> Handle(GetListQuery query, CancellationToken cancellationToken)
        {
            if (_cacheService.TryGet(Constants.CacheKeys.CachedList, out List? cachedList) &&
                cachedList?.Id == query.Id)
            {
                return new Response<GetListResponse>(GetListResponse.From(cachedList));
            }

            var list = await _context.Lists.FirstOrDefaultAsync(l => l.Id == query.Id, cancellationToken);

            await _cacheService.SetAsync(Constants.CacheKeys.CachedList, list);

            return new Response<GetListResponse>(GetListResponse.From(list));
        }
    }
}