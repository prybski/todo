﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ToDo.Core.Application.Contexts.Interfaces;
using ToDo.Core.Application.Dtos.Responses;
using ToDo.Core.Application.Extensions;
using ToDo.Core.Application.Queries.ListQueries;
using ToDo.Core.Application.Wrappers.Responses;

namespace ToDo.Core.Application.QueryHandlers.ListQueryHandlers
{
    public class GetAllListsPaginatedQueryHandler : IRequestHandler<GetAllListsPaginatedQuery,
        PaginatedResponse<IEnumerable<GetListResponse>>>
    {
        private readonly IToDoDbContext _context;

        public GetAllListsPaginatedQueryHandler(IToDoDbContext context)
        {
            _context = context;
        }

        public async Task<PaginatedResponse<IEnumerable<GetListResponse>>> Handle(GetAllListsPaginatedQuery query,
            CancellationToken cancellationToken)
        {
            return await _context.Lists.OrderBy(l => l.Id).Select(l => GetListResponse.From(l))
                .AsPaginatedResponseAsync(query.Filter);
        }
    }
}