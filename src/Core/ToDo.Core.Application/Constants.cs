﻿namespace ToDo.Core.Application
{
    public static class Constants
    {
        public static class CacheKeys
        {
            public const string CachedList = nameof(CachedList);
            public const string CachedTask = nameof(CachedTask);
        }

        public static class ConnectionStrings
        {
            public const string PostgreSql = nameof(PostgreSql);
            public const string Redis = nameof(Redis);
        }
    }
}