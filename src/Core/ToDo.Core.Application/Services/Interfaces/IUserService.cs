using System.Threading.Tasks;
using ToDo.Core.Application.Dtos.Requests;
using ToDo.Core.Application.Dtos.Responses;
using ToDo.Core.Application.Wrappers.Responses;

namespace ToDo.Core.Application.Services.Interfaces
{
    public interface IUserService
    {
        Task<Response<AuthenticateResponse>> AuthenticateAsync(AuthenticateRequest request, string ipAddress);
        Task<Response<string>> ConfirmEmailAsync(ConfirmEmailRequest request);
        Task<Response<string>> ForgotPasswordAsync(ForgotPasswordRequest request, string resetPasswordUrl);
        Task<Response<AuthenticateResponse>> RefreshTokenAsync(string refreshToken, string ipAddress);
        Task<Response<string>> RegisterAsync(RegisterRequest request, string confirmEmailUrl);
        Task<Response<string>> ResetPasswordAsync(ResetPasswordRequest request);
    }
}