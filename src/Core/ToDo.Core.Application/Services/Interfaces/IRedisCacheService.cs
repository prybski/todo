﻿using System.Threading.Tasks;

namespace ToDo.Core.Application.Services.Interfaces
{
    public interface IRedisCacheService
    {
        Task RemoveAsync(string key);
        Task SetAsync<TValue>(string key, TValue value);
        bool TryGet<TValue>(string key, out TValue? value);
    }
}