﻿using System.Threading.Tasks;
using ToDo.Core.Application.Dtos.Requests;

namespace ToDo.Core.Application.Services.Interfaces
{
    public interface IEmailService
    {
        Task SendAsync(SendEmailRequest request);
    }
}