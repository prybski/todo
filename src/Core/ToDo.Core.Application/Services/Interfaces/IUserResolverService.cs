﻿namespace ToDo.Core.Application.Services.Interfaces
{
    public interface IUserResolverService
    {
        string GetUserId();
    }
}