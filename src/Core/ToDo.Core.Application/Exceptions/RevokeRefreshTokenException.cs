using System.Net;
using ToDo.Core.Application.Common.Exceptions;

namespace ToDo.Core.Application.Exceptions
{
    public class RevokeRefreshTokenException : ApiException
    {
        public RevokeRefreshTokenException(string message, HttpStatusCode statusCode) : base(message, statusCode)
        {
        }
    }
}