using System.Net;
using ToDo.Core.Application.Common.Exceptions;

namespace ToDo.Core.Application.Exceptions
{
    public class GetRefreshTokensException : ApiException
    {
        public GetRefreshTokensException(string message, HttpStatusCode statusCode) : base(message, statusCode)
        {
        }
    }
}