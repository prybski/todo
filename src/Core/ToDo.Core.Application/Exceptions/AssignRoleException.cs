using System.Net;
using ToDo.Core.Application.Common.Exceptions;

namespace ToDo.Core.Application.Exceptions
{
    public class AssignRoleException : ApiException
    {
        public AssignRoleException(string message, HttpStatusCode statusCode) : base(message, statusCode)
        {
        }
    }
}