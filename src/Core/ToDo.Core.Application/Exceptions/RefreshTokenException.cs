using System.Net;
using ToDo.Core.Application.Common.Exceptions;

namespace ToDo.Core.Application.Exceptions
{
    public class RefreshTokenException : ApiException
    {
        public RefreshTokenException(string message, HttpStatusCode statusCode) : base(message, statusCode)
        {
        }
    }
}