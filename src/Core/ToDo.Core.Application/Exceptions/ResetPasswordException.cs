using System.Collections.Generic;
using System.Net;
using ToDo.Core.Application.Common.Exceptions;

namespace ToDo.Core.Application.Exceptions
{
    public class ResetPasswordException : ApiException
    {
        public ResetPasswordException(string message, HttpStatusCode statusCode) : base(message, statusCode)
        {
        }

        public ResetPasswordException(string message, HttpStatusCode statusCode, IEnumerable<string> errors) : base(
            message, statusCode, errors)
        {
        }
    }
}