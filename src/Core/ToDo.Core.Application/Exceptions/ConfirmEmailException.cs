﻿using System.Net;
using ToDo.Core.Application.Common.Exceptions;

namespace ToDo.Core.Application.Exceptions
{
    public class ConfirmEmailException : ApiException
    {
        public ConfirmEmailException(string message, HttpStatusCode statusCode) : base(message, statusCode)
        {
        }
    }
}