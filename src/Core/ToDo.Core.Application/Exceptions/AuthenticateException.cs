using System.Net;
using ToDo.Core.Application.Common.Exceptions;

namespace ToDo.Core.Application.Exceptions
{
    public class AuthenticateException : ApiException
    {
        public AuthenticateException(string message, HttpStatusCode statusCode) : base(message, statusCode)
        {
        }
    }
}