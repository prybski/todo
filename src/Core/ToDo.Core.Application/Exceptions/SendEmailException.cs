﻿using System.Net;
using ToDo.Core.Application.Common.Exceptions;

namespace ToDo.Core.Application.Exceptions
{
    public class SendEmailException : ApiException
    {
        public SendEmailException(string message, HttpStatusCode statusCode) : base(message, statusCode)
        {
        }
    }
}