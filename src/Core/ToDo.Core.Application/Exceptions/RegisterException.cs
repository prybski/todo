using System.Collections.Generic;
using System.Net;
using ToDo.Core.Application.Common.Exceptions;

namespace ToDo.Core.Application.Exceptions
{
    public class RegisterException : ApiException
    {
        public RegisterException(string message, HttpStatusCode statusCode) : base(message, statusCode)
        {
        }

        public RegisterException(string message, HttpStatusCode statusCode, IEnumerable<string> errors) : base(message,
            statusCode, errors)
        {
        }
    }
}