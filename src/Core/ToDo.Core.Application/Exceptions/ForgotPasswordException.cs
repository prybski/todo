﻿using System.Net;
using ToDo.Core.Application.Common.Exceptions;

namespace ToDo.Core.Application.Exceptions
{
    public class ForgotPasswordException : ApiException
    {
        public ForgotPasswordException(string message, HttpStatusCode statusCode) : base(message, statusCode)
        {
        }
    }
}