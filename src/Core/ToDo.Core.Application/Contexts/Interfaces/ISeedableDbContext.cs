using System.Collections.Generic;
using ToDo.Core.Application.Contexts.Seeds;

namespace ToDo.Core.Application.Contexts.Interfaces
{
    public interface ISeedableDbContext
    {
        ICollection<DbContextSeed> ContextSeeds { get; }
    }
}