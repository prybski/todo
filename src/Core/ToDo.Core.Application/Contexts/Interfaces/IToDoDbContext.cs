﻿using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using ToDo.Core.Domain.Entities;

using Task = ToDo.Core.Domain.Entities.Task;

namespace ToDo.Core.Application.Contexts.Interfaces
{
    public interface IToDoDbContext
    {
        DbSet<List> Lists { get; }
        DbSet<Task> Tasks { get; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}