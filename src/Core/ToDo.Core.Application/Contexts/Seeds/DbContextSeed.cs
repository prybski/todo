using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace ToDo.Core.Application.Contexts.Seeds
{
    public abstract class DbContextSeed
    {
        public IServiceScope? Scope { get; set; }

        public abstract Task ExecuteAsync();
    }
}