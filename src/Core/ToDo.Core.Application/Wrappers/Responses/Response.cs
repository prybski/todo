using System.Collections.Generic;
using System.Linq;

namespace ToDo.Core.Application.Wrappers.Responses
{
    public record Response<TData>(bool WasSuccessful, string? Message, TData? Data, IEnumerable<string> Errors)
        where TData : class
    {
        protected Response() : this(default, default, default, Enumerable.Empty<string>())
        {
        }

        public Response(string? message) : this()
        {
            WasSuccessful = false;
            Message = message;
        }

        public Response(TData? data, string? message = null) : this()
        {
            WasSuccessful = true;
            Message = message;

            Data = data;
        }
    }
}