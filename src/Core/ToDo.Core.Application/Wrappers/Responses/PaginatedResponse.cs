using System.Collections;

namespace ToDo.Core.Application.Wrappers.Responses
{
    public record PaginatedResponse<TData>
        (int PageNumber, int PageSize, int TotalItems, int TotalPages) : Response<TData>
        where TData : class, IEnumerable
    {
        public PaginatedResponse(int pageNumber, int pageSize, int totalItems, int totalPages, TData? data) : this(
            pageNumber, pageSize, totalItems, totalPages)
        {
            WasSuccessful = true;

            PageNumber = pageNumber;
            PageSize = pageSize;

            TotalItems = totalItems;
            TotalPages = totalPages;

            Data = data;
        }
    }
}