using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo.Core.Application.Filters;
using ToDo.Core.Application.Wrappers.Responses;

namespace ToDo.Core.Application.Extensions
{
    internal static class QueryableExtensions
    {
        public static async Task<PaginatedResponse<IEnumerable<TData>>> AsPaginatedResponseAsync<TData>(
            this IQueryable<TData> source, PaginationFilter filter) where TData : class
        {
            var (pageNumber, pageSize) = filter;

            var entities = await source.Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            var totalItems = await source.CountAsync();

            var totalPages = (int) Math.Ceiling(totalItems / (double) pageSize);

            var paginatedResponse =
                new PaginatedResponse<IEnumerable<TData>>(pageNumber, pageSize, totalItems,
                    totalPages == 0 ? 1 : totalPages, entities);

            return paginatedResponse;
        }
    }
}