using System.ComponentModel.DataAnnotations;

namespace ToDo.Core.Application.Dtos.Requests
{
    public record AuthenticateRequest([Required] [EmailAddress] string Email, [Required] string Password);
}