﻿namespace ToDo.Core.Application.Dtos.Requests
{
    public record SendEmailRequest(string To, string Subject, string Body);
}