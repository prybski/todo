using System.ComponentModel.DataAnnotations;

namespace ToDo.Core.Application.Dtos.Requests
{
    public record RegisterRequest([Required] [MinLength(4)] string UserName, [Required] [EmailAddress] string Email,
        [Required] [MinLength(6)] string Password);
}