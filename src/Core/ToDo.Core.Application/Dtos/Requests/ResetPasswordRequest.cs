using System.ComponentModel.DataAnnotations;

namespace ToDo.Core.Application.Dtos.Requests
{
    public record ResetPasswordRequest([Required] [EmailAddress] string Email, [Required] string Token,
        [Required] [MinLength(6)] string NewPassword);
}