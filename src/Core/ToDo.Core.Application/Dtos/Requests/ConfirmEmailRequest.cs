﻿namespace ToDo.Core.Application.Dtos.Requests
{
    public record ConfirmEmailRequest(string UserId, string Token);
}