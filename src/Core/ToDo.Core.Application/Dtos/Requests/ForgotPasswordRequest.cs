﻿using System.ComponentModel.DataAnnotations;

namespace ToDo.Core.Application.Dtos.Requests
{
    public record ForgotPasswordRequest([Required] [EmailAddress] string Email);
}