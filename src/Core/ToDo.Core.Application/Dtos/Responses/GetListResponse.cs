﻿using ToDo.Core.Domain.Entities;

namespace ToDo.Core.Application.Dtos.Responses
{
    public record GetListResponse(int Id, string Name)
    {
        public static GetListResponse From(List list)
        {
            return new(list.Id, list.Name);
        }
    }
}