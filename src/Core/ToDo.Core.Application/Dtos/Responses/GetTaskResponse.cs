﻿using ToDo.Core.Domain.Entities;
using ToDo.Core.Domain.Enums;

namespace ToDo.Core.Application.Dtos.Responses
{
    public record GetTaskResponse(int Id, string Title, string Description, TaskType? Type)
    {
        public static GetTaskResponse From(Task task)
        {
            return new(task.Id, task.Title, task.Description, task.Type);
        }
    }
}