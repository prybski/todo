using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ToDo.Core.Application.Dtos.Responses
{
    public record AuthenticateResponse(string Id, string UserName, string Email, bool IsAuthenticated, string Token,
        DateTime? RefreshTokenExpirationDate,
        List<string> Roles)
    {
        [JsonIgnore] public string? RefreshToken { get; init; }

        public AuthenticateResponse(string id, string userName, string email, bool isAuthenticated, string token,
            string? refreshToken, DateTime refreshTokenExpirationDate, List<string> roles)
            : this(id, userName, email, isAuthenticated, token, refreshTokenExpirationDate, roles)
        {
            Id = id;
            UserName = userName;
            Email = email;
            IsAuthenticated = isAuthenticated;
            Token = token;
            RefreshToken = refreshToken;
            RefreshTokenExpirationDate = refreshTokenExpirationDate;
            Roles = roles;
        }
    }
}