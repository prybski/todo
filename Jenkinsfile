pipeline {
  agent any

  environment {
    JOB_PROJECT_NAME = "${(JOB_NAME.tokenize('/') as String[])[0]}"
  }

  stages {
    stage('Test build') {
      when {
        anyOf {
          branch 'stage'
          branch 'develop'

          expression { env.TAG_NAME =~ /^(v\d+.?\d*)-uat$/ }
        }
      }

      steps {
        sh 'dotnet restore'

        sh 'dotnet build'
      }
    }

    stage('Build and push image (development)') {
      when {
        branch 'develop'
      }

      steps {
        withCredentials([
          file(credentialsId: '6e11e1f2-c761-4d61-8dbd-4fea76c867bc', variable: 'GCLOUD_CONTAINER_REGISTRY_KEY_FILE')
        ]) {
          sh '''
            cat ${GCLOUD_CONTAINER_REGISTRY_KEY_FILE} | docker login ${GCLOUD_CONTAINER_REGISTRY} --username _json_key \
              --password-stdin
          '''
        }

        sh '''
          docker build . \
            --tag ${GCLOUD_CONTAINER_REGISTRY}/${GCLOUD_PROJECT_ID}/${JOB_PROJECT_NAME}:${GIT_COMMIT}-development
        '''

        sh "docker push ${GCLOUD_CONTAINER_REGISTRY}/${GCLOUD_PROJECT_ID}/${JOB_PROJECT_NAME}:${GIT_COMMIT}-development"

        sh "docker logout ${GCLOUD_CONTAINER_REGISTRY}"
      }
    }

    stage('Build and push image (staging)') {
      when {
        branch 'stage'
      }

      steps {
        withCredentials([
          file(credentialsId: '6e11e1f2-c761-4d61-8dbd-4fea76c867bc', variable: 'GCLOUD_CONTAINER_REGISTRY_KEY_FILE')
        ]) {
          sh '''
            cat ${GCLOUD_CONTAINER_REGISTRY_KEY_FILE} | docker login ${GCLOUD_CONTAINER_REGISTRY} --username _json_key \
              --password-stdin
          '''
        }

        sh '''
          docker build . \
            --tag ${GCLOUD_CONTAINER_REGISTRY}/${GCLOUD_PROJECT_ID}/${JOB_PROJECT_NAME}:${GIT_COMMIT}-staging
        '''

        sh "docker push ${GCLOUD_CONTAINER_REGISTRY}/${GCLOUD_PROJECT_ID}/${JOB_PROJECT_NAME}:${GIT_COMMIT}-staging"

        sh "docker logout ${GCLOUD_CONTAINER_REGISTRY}"
      }
    }

    stage('Build and push image (uat)') {
      when {
        expression { env.TAG_NAME =~ /^(v\d+.?\d*)-uat$/ }
      }

      steps {
        withCredentials([
          file(credentialsId: '6e11e1f2-c761-4d61-8dbd-4fea76c867bc', variable: 'GCLOUD_CONTAINER_REGISTRY_KEY_FILE')
        ]) {
          sh '''
            cat ${GCLOUD_CONTAINER_REGISTRY_KEY_FILE} | docker login ${GCLOUD_CONTAINER_REGISTRY} --username _json_key \
              --password-stdin
          '''
        }

        sh "docker build . --tag ${GCLOUD_CONTAINER_REGISTRY}/${GCLOUD_PROJECT_ID}/${JOB_PROJECT_NAME}:${TAG_NAME}"

        sh "docker push ${GCLOUD_CONTAINER_REGISTRY}/${GCLOUD_PROJECT_ID}/${JOB_PROJECT_NAME}:${TAG_NAME}"

        sh "docker logout ${GCLOUD_CONTAINER_REGISTRY}"
      }
    }

    stage('Build and push image (production)') {
      when {
        expression { env.TAG_NAME =~ /^(v\d+.?\d*)-production$/ }
      }

      steps {
        withCredentials([
          file(credentialsId: '6e11e1f2-c761-4d61-8dbd-4fea76c867bc', variable: 'GCLOUD_CONTAINER_REGISTRY_KEY_FILE')
        ]) {
          sh '''
            cat ${GCLOUD_CONTAINER_REGISTRY_KEY_FILE} | docker login ${GCLOUD_CONTAINER_REGISTRY} --username _json_key \
              --password-stdin
          '''
        }

        sh "docker build . --tag ${GCLOUD_CONTAINER_REGISTRY}/${GCLOUD_PROJECT_ID}/${JOB_PROJECT_NAME}:${TAG_NAME}"

        sh "docker push ${GCLOUD_CONTAINER_REGISTRY}/${GCLOUD_PROJECT_ID}/${JOB_PROJECT_NAME}:${TAG_NAME}"

        sh "docker logout ${GCLOUD_CONTAINER_REGISTRY}"
      }
    }

    stage('Deploy (development)') {
      when {
        branch 'develop'
      }

      steps {
        withCredentials([
          file(credentialsId: '7d329a49-4c15-4441-9e7c-355e59180b55', variable: 'GCLOUD_HELM_PACKAGE_MANAGER_KEY_FILE')
        ]) {
          sh "gcloud auth activate-service-account --key-file \${GCLOUD_HELM_PACKAGE_MANAGER_KEY_FILE}"
        }

        sh '''
          gcloud container clusters get-credentials ${GCLOUD_CLUSTER_NAME} --region ${GCLOUD_CLUSTER_REGION} \
            --project ${GCLOUD_PROJECT_ID}
        '''

        withCredentials([
          usernamePassword(credentialsId: '0a5b28bb-c113-4efe-bdad-7cd42b4511a3', usernameVariable: 'EMAIL_USERNAME',
            passwordVariable: 'EMAIL_PASSWORD'),
          string(credentialsId: 'f855b820-0785-4210-b5cb-896b25f55b7b', variable: 'JWT_KEY'),
          string(credentialsId: 'b59cae85-ec48-4e23-b913-676ce917553b', variable: 'POSTGRESQL_PASSWORD')
        ]) {
          sh '''
            helm upgrade ${JOB_PROJECT_NAME} charts/${JOB_PROJECT_NAME} --install --namespace development \
              --create-namespace \
              --set aspNetCoreEnvironment=Development \
              --set emailConfig.username=${EMAIL_USERNAME} \
              --set emailConfig.password="${EMAIL_PASSWORD}" \
              --set frontendConfig.host=35.234.146.145 \
              --set frontendConfig.port=1080 \
              --set image.tag=${GIT_COMMIT}-development \
              --set jwtConfig.key="${JWT_KEY}" \
              --set kibana.elasticsearch.hosts[0]=${JOB_PROJECT_NAME}-elasticsearch-coordinating-only.development.svc.cluster.local \
              --set kibana.service.type=LoadBalancer \
              --set kibana.service.loadBalancerIP=35.234.146.145 \
              --set kibana.service.port=1601 \
              --set postgresql.global.postgresql.postgresqlUsername=default-development \
              --set postgresql.global.postgresql.postgresqlPassword=${POSTGRESQL_PASSWORD} \
              --set service.loadBalancerIP=35.234.146.145 \
              --set service.port=1000
          '''
        }
      }
    }

    stage('Deploy (staging)') {
      when {
        branch 'stage'
      }

      steps {
        withCredentials([
          file(credentialsId: '7d329a49-4c15-4441-9e7c-355e59180b55', variable: 'GCLOUD_HELM_PACKAGE_MANAGER_KEY_FILE')
        ]) {
          sh "gcloud auth activate-service-account --key-file \${GCLOUD_HELM_PACKAGE_MANAGER_KEY_FILE}"
        }

        sh '''
          gcloud container clusters get-credentials ${GCLOUD_CLUSTER_NAME} --region ${GCLOUD_CLUSTER_REGION} \
            --project ${GCLOUD_PROJECT_ID}
        '''

        withCredentials([
          usernamePassword(credentialsId: '0a5b28bb-c113-4efe-bdad-7cd42b4511a3', usernameVariable: 'EMAIL_USERNAME',
            passwordVariable: 'EMAIL_PASSWORD'),
          string(credentialsId: 'f855b820-0785-4210-b5cb-896b25f55b7b', variable: 'JWT_KEY'),
          string(credentialsId: 'b59cae85-ec48-4e23-b913-676ce917553b', variable: 'POSTGRESQL_PASSWORD')
        ]) {
          sh '''
            helm upgrade ${JOB_PROJECT_NAME} charts/${JOB_PROJECT_NAME} --install --namespace staging \
              --create-namespace \
              --set aspNetCoreEnvironment=Staging \
              --set emailConfig.username=${EMAIL_USERNAME} \
              --set emailConfig.password="${EMAIL_PASSWORD}" \
              --set frontendConfig.host=35.234.146.145 \
              --set frontendConfig.port=2080 \
              --set image.tag=${GIT_COMMIT}-staging \
              --set jwtConfig.key="${JWT_KEY}" \
              --set kibana.elasticsearch.hosts[0]=${JOB_PROJECT_NAME}-elasticsearch-coordinating-only.staging.svc.cluster.local \
              --set kibana.service.type=LoadBalancer \
              --set kibana.service.loadBalancerIP=35.234.146.145 \
              --set kibana.service.port=2601 \
              --set postgresql.global.postgresql.postgresqlUsername=default-staging \
              --set postgresql.global.postgresql.postgresqlPassword=${POSTGRESQL_PASSWORD} \
              --set service.loadBalancerIP=35.234.146.145 \
              --set service.port=2000
          '''
        }
      }
    }

    stage('Deploy (uat)') {
      when {
        expression { env.TAG_NAME =~ /^(v\d+.?\d*)-uat$/ }
      }

      steps {
        withCredentials([
          file(credentialsId: '7d329a49-4c15-4441-9e7c-355e59180b55', variable: 'GCLOUD_HELM_PACKAGE_MANAGER_KEY_FILE')
        ]) {
          sh "gcloud auth activate-service-account --key-file \${GCLOUD_HELM_PACKAGE_MANAGER_KEY_FILE}"
        }

        sh '''
          gcloud container clusters get-credentials ${GCLOUD_CLUSTER_NAME} --region ${GCLOUD_CLUSTER_REGION} \
            --project ${GCLOUD_PROJECT_ID}
        '''

        withCredentials([
          usernamePassword(credentialsId: '0a5b28bb-c113-4efe-bdad-7cd42b4511a3', usernameVariable: 'EMAIL_USERNAME',
            passwordVariable: 'EMAIL_PASSWORD'),
          string(credentialsId: 'f855b820-0785-4210-b5cb-896b25f55b7b', variable: 'JWT_KEY'),
          string(credentialsId: 'b59cae85-ec48-4e23-b913-676ce917553b', variable: 'POSTGRESQL_PASSWORD')
        ]) {
          sh '''
            helm upgrade ${JOB_PROJECT_NAME} charts/${JOB_PROJECT_NAME} --install --namespace uat --create-namespace \
              --set aspNetCoreEnvironment=UAT \
              --set emailConfig.username=${EMAIL_USERNAME} \
              --set emailConfig.password="${EMAIL_PASSWORD}" \
              --set frontendConfig.host=35.234.146.145 \
              --set frontendConfig.port=3080 \
              --set image.tag=${TAG_NAME} \
              --set jwtConfig.key="${JWT_KEY}" \
              --set kibana.elasticsearch.hosts[0]=${JOB_PROJECT_NAME}-elasticsearch-coordinating-only.uat.svc.cluster.local \
              --set kibana.service.type=LoadBalancer \
              --set kibana.service.loadBalancerIP=35.234.146.145 \
              --set kibana.service.port=3601 \
              --set postgresql.global.postgresql.postgresqlUsername=default-uat \
              --set postgresql.global.postgresql.postgresqlPassword=${POSTGRESQL_PASSWORD} \
              --set service.loadBalancerIP=35.234.146.145 \
              --set service.port=3000
          '''
        }
      }
    }

    stage('Deploy (production)') {
      when {
        expression { env.TAG_NAME =~ /^(v\d+.?\d*)-production$/ }
      }

      steps {
        withCredentials([
          file(credentialsId: '7d329a49-4c15-4441-9e7c-355e59180b55', variable: 'GCLOUD_HELM_PACKAGE_MANAGER_KEY_FILE')
        ]) {
          sh "gcloud auth activate-service-account --key-file \${GCLOUD_HELM_PACKAGE_MANAGER_KEY_FILE}"
        }

        sh '''
          gcloud container clusters get-credentials ${GCLOUD_CLUSTER_NAME} --region ${GCLOUD_CLUSTER_REGION} \
            --project ${GCLOUD_PROJECT_ID}
        '''

        withCredentials([
          usernamePassword(credentialsId: '0a5b28bb-c113-4efe-bdad-7cd42b4511a3', usernameVariable: 'EMAIL_USERNAME',
            passwordVariable: 'EMAIL_PASSWORD'),
          string(credentialsId: 'f855b820-0785-4210-b5cb-896b25f55b7b', variable: 'JWT_KEY'),
          string(credentialsId: 'b59cae85-ec48-4e23-b913-676ce917553b', variable: 'POSTGRESQL_PASSWORD')
        ]) {
          sh '''
            helm upgrade ${JOB_PROJECT_NAME} charts/${JOB_PROJECT_NAME} --install --namespace production \
              --create-namespace \
              --set aspNetCoreEnvironment=Production \
              --set emailConfig.username=${EMAIL_USERNAME} \
              --set emailConfig.password="${EMAIL_PASSWORD}" \
              --set frontendConfig.host=35.234.146.145 \
              --set frontendConfig.port=4080 \
              --set image.tag=${TAG_NAME} \
              --set jwtConfig.key="${JWT_KEY}" \
              --set kibana.elasticsearch.hosts[0]=${JOB_PROJECT_NAME}-elasticsearch-coordinating-only.production.svc.cluster.local \
              --set kibana.service.type=LoadBalancer \
              --set kibana.service.loadBalancerIP=35.234.146.145 \
              --set kibana.service.port=4601 \
              --set postgresql.global.postgresql.postgresqlUsername=default-production \
              --set postgresql.global.postgresql.postgresqlPassword=${POSTGRESQL_PASSWORD} \
              --set service.loadBalancerIP=35.234.146.145 \
              --set service.port=4000
          '''
        }
      }
    }

    stage('Deploy frontend (development)') {
      when {
        branch 'develop'
      }

      steps {
        build job: "../${JOB_PROJECT_NAME}-frontend/${BRANCH_NAME}", wait: true
      }
    }

    stage('Deploy frontend (staging)') {
      when {
        branch 'stage'
      }

      steps {
        build job: "../${JOB_PROJECT_NAME}-frontend/${BRANCH_NAME}", wait: true
      }
    }

    stage('Deploy frontend (uat)') {
      when {
        expression { env.TAG_NAME =~ /^(v\d+.?\d*)-uat$/ }
      }

      steps {
        build job: "../${JOB_PROJECT_NAME}-frontend/${TAG_NAME}", wait: true
      }
    }

    stage('Deploy frontend (production)') {
      when {
        expression { env.TAG_NAME =~ /^(v\d+.?\d*)-production$/ }
      }

      steps {
        build job: "../${JOB_PROJECT_NAME}-frontend/${TAG_NAME}", wait: true
      }
    }
  }

  post {
    cleanup {
      cleanWs()
    }
  }
}
