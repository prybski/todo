FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build-env
WORKDIR /app

EXPOSE 5000

COPY . ./
RUN dotnet restore

RUN dotnet publish --configuration Release --output out

FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app

ENV ASPNETCORE_URLS http://+:5000

COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "ToDo.Presentation.Web.Api.dll"]